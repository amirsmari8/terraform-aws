# RDS Ressources 
resource "aws_db_subnet_group" "postgres_subnets" {
  name = "postgres_subnets" 
  subnet_ids = [aws_subnet.dev_vpc-private-1.id, aws_subnet.dev_vpc-private-2.id]
}



resource "aws_db_instance" "postgres" {
  allocated_storage    = 10
  engine               = "postgres"
  engine_version       = "13.4"
  instance_class       = "db.t3.micro"
  name                 = "mydb"
  username             = "amir"
  password             = "amir1234"   #must be more than 8car
  vpc_security_group_ids = [aws_security_group.SG-database.id]
  db_subnet_group_name = aws_db_subnet_group.postgres_subnets.name
  multi_az = "false" # set to true to have high availability: 2 instances synchronized with each other ()
  availability_zone = aws_subnet.dev_vpc-private-1.availability_zone #prefered AZ
  skip_final_snapshot  = true

}