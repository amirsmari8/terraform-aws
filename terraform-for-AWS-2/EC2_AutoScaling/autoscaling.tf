# Auto-Scaling Lunch Configuration 
resource "aws_launch_configuration" "dev_lunch_configuration" {
  name          = "dev_lunch_configuration"
  instance_type = "t2.micro"
  image_id = lookup(var.AMI, var.REGION) 
#   key_name = aws_key_pair.mykey.key_name
}

# Auto Scaling Group 
resource "aws_autoscaling_group" "auto_scaling" {
  name                 = "dev_auto_scaling"
  launch_configuration = aws_launch_configuration.dev_lunch_configuration.name
  min_size             = 1
  max_size             = 2
  health_check_grace_period = 200  // after 200 secondes
  health_check_type = "EC2"
  force_delete = true
  availability_zones = ["us-east-2a", "us-east-2b"]

}

# Autoscaling configuration policy 
resource "aws_autoscaling_policy" "aws_autoscaling_policy" {
  name                   = "aws_autoscaling_policy"
  scaling_adjustment     = 1   // means scale the node one by one 
  adjustment_type        = "ChangeInCapacity"
  cooldown               = 200   // cooldown periode
  autoscaling_group_name = aws_autoscaling_group.auto_scaling.name 
}

#Auto scaling Cloud Watch Monitoring
resource "aws_cloudwatch_metric_alarm" "cpu-alarm" {
  alarm_name          = "cpu-alarm"
  alarm_description   = "Alarm once CPU Uses Increase"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "2"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/EC2"
  period              = "120"
  statistic           = "Average"
  threshold           = "30" 

  dimensions = {
    "AutoScalingGroupName" = aws_autoscaling_group.auto_scaling.name
  }

  actions_enabled = true
  alarm_actions   = [aws_autoscaling_policy.aws_autoscaling_policy.arn]
}



#                                        we need to define also how to descale policy (for cost purpose) 

resource "aws_autoscaling_policy" "aws_autoscaling_policy-scaledown" {
  name                   = "aws_autoscaling_policy-scaledown"
  scaling_adjustment     = -1   // means scale down the node one by one 
  adjustment_type        = "ChangeInCapacity"
  cooldown               = 200   // cooldown periode
  autoscaling_group_name = aws_autoscaling_group.auto_scaling.name 
}

resource "aws_cloudwatch_metric_alarm" "cpu-alarm-scaledown" {
  alarm_name          = "cpu-alarm-scaledown"
  alarm_description   = "Alarm once CPU Uses decrease"
  comparison_operator = "LessThanOrEqualToThreshold"
  evaluation_periods  = "2"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/EC2"
  period              = "120"
  statistic           = "Average"
  threshold           = "10"

  dimensions = {
    "AutoScalingGroupName" = aws_autoscaling_group.auto_scaling.name
  }

  actions_enabled = true
  alarm_actions   = [aws_autoscaling_policy.aws_autoscaling_policy-scaledown.arn]
}
