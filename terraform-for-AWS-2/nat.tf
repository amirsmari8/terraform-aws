resource "aws_eip" "eip" {
  vpc      = true
}


# NAT Gateway
resource "aws_nat_gateway" "Nat-Gateway" {
  allocation_id = aws_eip.eip.id
  subnet_id     = aws_subnet.dev_vpc-public-1.id

  tags = {
    Name = "gw NAT"
  }

  # To ensure proper ordering, it is recommended to add an explicit dependency
  # on the Internet Gateway for the VPC.
  depends_on = [aws_internet_gateway.dev_gw]
}


# route table private for  Custom VPC
resource "aws_route_table" "dev_route_table_private" {
  vpc_id = aws_vpc.dev_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.Nat-Gateway.id
  }

  tags = {
    Name = "dev_route_table_private"
  }
}

resource "aws_route_table_association" "dev-private-2-a" {
  subnet_id      = aws_subnet.dev_vpc-private-1.id
  route_table_id = aws_route_table.dev_route_table_private.id
}

resource "aws_route_table_association" "dev-private-2-b" {
  subnet_id      = aws_subnet.dev_vpc-private-2.id
  route_table_id = aws_route_table.dev_route_table_private.id
}

resource "aws_route_table_association" "dev-private-2-c" {
  subnet_id      = aws_subnet.dev_vpc-private-3.id
  route_table_id = aws_route_table.dev_route_table_private.id
}