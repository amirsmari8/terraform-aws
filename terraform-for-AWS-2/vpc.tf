# CUSTOM VPC
resource "aws_vpc" "dev_vpc" {
    cidr_block = "10.0.0.0/16" 
    instance_tenancy = "default" 
    enable_dns_support = "true"
    enable_dns_hostnames = "true"
    enable_classiclink = "false"

    tags = {
      Name = "dev_vpc"
    }
  
}

# Custom public Subnet 
resource "aws_subnet" "dev_vpc-public-1" {
    vpc_id = aws_vpc.dev_vpc.id 
    cidr_block = "10.0.1.0/24"
    map_public_ip_on_launch = "true"  #whenever an ec2 is lunched is this subnet , give it a public IP address
    availability_zone = "us-east-2a"
    tags = {
      Name = "dev_vpc-public-1"
    }
}

resource "aws_subnet" "dev_vpc-public-2" {
    vpc_id = aws_vpc.dev_vpc.id 
    cidr_block = "10.0.2.0/24"
    map_public_ip_on_launch = "true"  #whenever an ec2 is lunched is this subnet , give it a public IP address
    availability_zone = "us-east-2b"
    tags = {
      Name = "dev_vpc-public-2"
    }
}

resource "aws_subnet" "dev_vpc-public-3" {
    vpc_id = aws_vpc.dev_vpc.id 
    cidr_block = "10.0.3.0/24"
    map_public_ip_on_launch = "true"  #whenever an ec2 is lunched is this subnet , give it a public IP address
    availability_zone = "us-east-2c"
    tags = {
      Name = "dev_vpc-public-3"
    }
}

# Custom private Subnet 
resource "aws_subnet" "dev_vpc-private-1" {
    vpc_id = aws_vpc.dev_vpc.id 
    cidr_block = "10.0.4.0/24"
    map_public_ip_on_launch = "false"  
    availability_zone = "us-east-2a"
    tags = {
      Name = "dev_vpc-private-1"
    }
}

resource "aws_subnet" "dev_vpc-private-2" {
    vpc_id = aws_vpc.dev_vpc.id 
    cidr_block = "10.0.5.0/24"
    map_public_ip_on_launch = "false"  
    availability_zone = "us-east-2b"
    tags = {
      Name = "dev_vpc-private-2"
    }
}

resource "aws_subnet" "dev_vpc-private-3" {
    vpc_id = aws_vpc.dev_vpc.id 
    cidr_block = "10.0.6.0/24"
    map_public_ip_on_launch = "false"  
    availability_zone = "us-east-2c"
    tags = {
      Name = "dev_vpc-private-3"
    }
}

# Custom Internet Gateway

resource "aws_internet_gateway" "dev_gw" {
  vpc_id = aws_vpc.dev_vpc.id
  tags = {
    Name = "dev_gw"
  }
}

# route table public for  Custom VPC 
resource "aws_route_table" "dev_route_table_public" {
  vpc_id = aws_vpc.dev_vpc.id

  route {
    cidr_block = "0.0.0.0/0"                   // this entry sends all other subnet traffic to the NAT gateway
    gateway_id = aws_internet_gateway.dev_gw.id
  }
  tags = {
    Name = "dev_route_table_public"
  }
}    

# route table association
resource "aws_route_table_association" "dev-public-2-a" {
  subnet_id      = aws_subnet.dev_vpc-public-1.id
  route_table_id = aws_route_table.dev_route_table_public.id
}

resource "aws_route_table_association" "dev-public-2-b" {
  subnet_id      = aws_subnet.dev_vpc-public-2.id
  route_table_id = aws_route_table.dev_route_table_public.id
}

resource "aws_route_table_association" "dev-public-2-c" {
  subnet_id      = aws_subnet.dev_vpc-public-3.id
  route_table_id = aws_route_table.dev_route_table_public.id
}

