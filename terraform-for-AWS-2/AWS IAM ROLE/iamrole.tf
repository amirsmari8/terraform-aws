#role for ec2 == S3fullAccess
resource "aws_iam_role" "s3_role" {
  name = "s3_role"

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  assume_role_policy = jsonencode({    // role for ec2 == S3fullAccess
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "sts:AssumeRole"
            ],
            "Principal": {
                "Service": [
                    "ec2.amazonaws.com"
                ]
            }
        }
    ]
  })
}


#Policy to attach the s3 bucket roles
resource "aws_iam_role_policy" "s3-role-policy" {    //bucket-policy
  name = "s3-role-policy"
  role = aws_iam_role.s3_role.id
  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
              "s3:*"
            ],
            "Resource": [
              "arn:aws:s3:::amir_test_s3_bucket",
              "arn:aws:s3:::amir_test_s3_bucket/*"
            ]
        }
    ]
}
EOF

}
#Instance identifier
resource "aws_iam_instance_profile" "s3-role-instanceprofile" {
  name = "s3-role"
  role = aws_iam_role.s3_role.name
}