resource "aws_s3_bucket" "s3_bucket" {
  bucket = "amir_test_s3_bucket"
  acl = "private"
  tags = {
    Name        = "amir_test_s3_bucket"
  }
}

