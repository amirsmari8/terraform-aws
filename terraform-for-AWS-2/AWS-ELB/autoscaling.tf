# Auto-Scaling Lunch Configuration 
resource "aws_launch_configuration" "dev_lunch_configuration" {
  name          = "dev_lunch_configuration"
  instance_type = "t2.micro"
  image_id = lookup(var.AMI, var.REGION) 
  security_groups = [aws_security_group.SG-ec2.id]
}

# Auto Scaling Group 
resource "aws_autoscaling_group" "auto_scaling" {
  name                 = "dev_auto_scaling"
  launch_configuration = aws_launch_configuration.dev_lunch_configuration.name
  min_size             = 2
  max_size             = 2
  health_check_grace_period = 200  // after 200 secondes
  health_check_type = "ELB"
  force_delete = true
  vpc_zone_identifier = [aws_subnet.dev_vpc-public-1.id, aws_subnet.dev_vpc-public-2.id]
  load_balancers = [aws_elb.dev_elb.name]
}

