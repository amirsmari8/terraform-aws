# Security Groups for the instance
locals {
  ports = [22]
}

resource "aws_security_group" "SG" {
  vpc_id = aws_vpc.dev_vpc.id
  name = "sg_5002"
  dynamic "ingress" {
      for_each = local.ports
      content {
          from_port = ingress.value
          to_port = ingress.value
          protocol = "tcp" 
          cidr_blocks = ["0.0.0.0/0"]
      }
  }
  egress {
      from_port = 0 
      to_port = 0 
      protocol = "-1"
      cidr_blocks = ["0.0.0.0/0"]
  }
}

////////////////////////////////////////////////// 
resource "aws_security_group" "SG-database" {
  name = "sg_5003" 
  vpc_id = aws_vpc.dev_vpc.id
  
  ingress {
    from_port = 5432
    to_port = 5432
    protocol = "tcp"
    security_groups = [aws_security_group.SG.id]
  }
  egress {
    from_port = 0 
    to_port = 0 
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = "allow POSTGRES"
  }
}