# variable "ACCESS_KEY" {}

# variable "SECRET_ACCESS_KEY" {}

variable "REGION" {
  default = "us-east-2"
}

variable "AMI" {
  type = map(any)
  default = {
    us-east-1 = "ami-01896de1f162f0ab7"
    eu-west-2 = "ami-01d912b5940be07a5"
    us-west-1 = "ami-0189702ff9c0b592f"
    us-east-2 = "ami-045137e8d34668746"
  }
}

variable "PATH_TO_PUBLIC_KEY" {
  default = "mykey.pub"
}

variable "PATH_TO_PRIVATE_KEY" {
  default = "mykey"
}

variable "INSTANCE_USERNAME" {
  default = "ubuntu"
}