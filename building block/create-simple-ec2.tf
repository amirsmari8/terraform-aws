data "aws_availability_zones" "available" {}

resource "aws_instance" "myec2" {
  # count         = 3 #we will have 3 instances
  ami           = lookup(var.AMI, var.REGION)
  instance_type = "t2.micro"
  key_name = aws_key_pair.mykey.key_name
  tags = {
    Name = "dev-instance" #dev-instance-1, dev-instance-2, dev-instance-3
  }
  security_groups = [aws_security_group.sg.name]
  availability_zone = data.aws_availability_zones.available.names[1]    // choose the AZ at index 1

  provisioner "file" {
  source = "installNginx.sh"
  destination = "/tmp/installNginx.sh"
  }

  provisioner "remote-exec" {
    inline = [
      # "sudo apt-get update -y",
      "chmod +x /tmp/installNginx.sh"
      # "sudo /tmp/installNginx.sh"
    ]
  }

  connection {
  type     = "ssh"
  user     = var.INSTANCE_USERNAME
  private_key = file(var.PATH_TO_PRIVATE_KEY)
  host     = self.public_ip
  }


  provisioner "local-exec" {
    command =  "echo ${aws_instance.myec2.public_ip} >> EC2.txt && echo ${aws_instance.myec2.id} >> EC2.txt && echo ${aws_instance.myec2.key_name} >> EC2.txt"          
  }

}

resource "aws_key_pair" "mykey" {
  key_name = "mykey"
  public_key = file(var.PATH_TO_PUBLIC_KEY)
}

//////////// BASIC SECURITY GROUP ///////////////////
# resource "aws_security_group" "SG" {
#   name = "sg-5000" 
  
#   ingress {
#     description = "description 0" 
#     from_port = 22
#     to_port = 22
#     protocol = "tcp"
#     cidr_blocks = ["0.0.0.0/0"]
#   }
#   ingress {
#     description = "description 1 " 
#     from_port = 80 
#     to_port = 80 
#     protocol = "tcp"
#     cidr_blocks = ["0.0.0.0/0"]
#   }
# }
locals {
  ports = [22, 80]
}
resource "aws_security_group" "sg" {
  name = "sg_5001"
  dynamic "ingress" {
    for_each = local.ports
    content {
      description = "description ${ingress.key}"
      from_port = ingress.value 
      to_port = ingress.value
      protocol = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }
  egress {
    from_port = 0
    to_port = 0 
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}
//////////////////// ADVANCED USAGE ///////////////////////
# locals {
#   rules = [{
#     description = "description 0",
#     port = 80,
#     cidr_blocks = ["0.0.0.0/0"],
#   },{
#     description = "description 1",
#     port = 81,
#     cidr_blocks = ["10.0.0.0/16"],
#   }]
# }
# resource "aws_security_group" "attrs" {
#   name        = "demo-attrs"
#   description = "demo-attrs"

#   dynamic "ingress" {
#     for_each = local.rules
#     content {
#       description = ingress.value.description
#       from_port   = ingress.value.port
#       to_port     = ingress.value.port
#       protocol    = "tcp"
#       cidr_blocks = ingress.value.cidr_blocks
#     }
#   }
# }

output "ip_add" {
    value = aws_instance.myec2.public_ip
}

output "key_name" {
  value = aws_instance.myec2.key_name
}  