resource "aws_vpc" "custom_vpc" {
  
  cidr_block =              var.CIDR_BLOCK 
  enable_dns_support =      var.ENABLE_DNS_SUPPORT 
  instance_tenancy =        var.INSTANCE_TENANCY
  enable_dns_hostnames =    var.ENABLE_DNS_HOSTNAMES 
  enable_classiclink =      var.ENABLE_CLASSICLINK 
  
  tags = {
    Name =                  var.VPC_NAME
    environment =           var.ENVIRONMENT
  }
}