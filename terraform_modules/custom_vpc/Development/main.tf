module "aws_vpc" {
  source = "../../custom_vpc"  
  
  REGION = "us-east-2" 
  CIDR_BLOCK = "10.0.0.0/16" 
  ENABLE_DNS_SUPPORT = "true" 
  ENABLE_DNS_HOSTNAMES = "true" 
  VPC_NAME = "vpc_for_dev"
}