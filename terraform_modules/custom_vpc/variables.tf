variable "REGION" {
  type        = string    
  default = "us-east-2"
}

variable "CIDR_BLOCK" {
  default = "0.0.0.0/0"
}

variable "INSTANCE_TENANCY" {
  default =   "default"
}

variable "ENABLE_DNS_SUPPORT" {
  default = true
} 

variable "ENABLE_DNS_HOSTNAMES" {
  type    = bool    
  default = false
}

variable "ENABLE_CLASSICLINK" {
  default = null
}

variable "VPC_NAME" {
  default = "" 
}

variable "ENVIRONMENT" {
  default = "development"
}
