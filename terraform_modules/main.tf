module "ec2_instance" {
  name = "ec2"   // name of the instance 
  source  = "terraform-aws-modules/ec2-instance/aws" // source from github 
  instance_type          = "t2.micro"
  ami = "ami-045137e8d34668746" 
  subnet_id = "subnet-16f2715a"
  tags = {
    Terraform   = "true"
    Environment = "dev"
  }
}