terraform {
  backend "s3" {
      bucket = "tf-state-9666tty"
      key    = "development/terraform_state" 
      region = "us-east-2"
  }
}