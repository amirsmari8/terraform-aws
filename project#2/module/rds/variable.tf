variable "REGION" {
  default = "us-east-2"
}

variable "ENVIRONMENT" {
  default = "development"
}

variable "ALLOCATED_STORAGE" {
  default = 20
}

variable "ENGINE" {
    default = "mysql"
}

variable "ENGINE_VERSION" {
  default = "5.7"
}

variable "PUBLICLY_ACCESSIBLE" {
  default = "true"
}

variable "INSTANCE_CLASS" {
  default = "db.t3.micro"
}


variable "USERNAME" {
  default = "amir" 
  sensitive = true 
}

variable "PASSWORD" {
  default = "footfoot"
  sensitive = true
} 

variable "DB_NAME" {
  default = "testdb"  
}

variable "CIDR_SEC_RDS" {
  default = "0.0.0.0/0"
}

variable "private-subnet-1-id" {
  default = ""
}

variable "private-subnet-2-id" {
  default = ""
}

variable "vpc_id" {
  default = ""
}