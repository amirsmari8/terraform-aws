provider "aws" {
  region = var.REGION
}

module "my-vpc" {
  source = "../vpc" 
  ENVIRONMENT = var.ENVIRONMENT
  REGION = var.REGION
}

resource "aws_db_subnet_group" "db_subnet_group" {
  name       = "${var.ENVIRONMENT}-subnetgroup"
  subnet_ids = [var.private-subnet-1-id, var.private-subnet-2-id]

  tags = {
    Name = "${var.ENVIRONMENT}-subnetgroup"
  }
}

resource "aws_db_instance" "db_instance" {
    allocated_storage    = var.ALLOCATED_STORAGE
    engine               = var.ENGINE
    engine_version = var.ENGINE_VERSION
    instance_class = var.INSTANCE_CLASS 
    username = var.USERNAME 
    password = var.PASSWORD 
    db_name = var.DB_NAME 
    publicly_accessible = var.PUBLICLY_ACCESSIBLE
    db_subnet_group_name = aws_db_subnet_group.db_subnet_group.name
    multi_az = false
    skip_final_snapshot = true
    vpc_security_group_ids = [aws_security_group.rds_security_group.id]

}

resource "aws_security_group" "rds_security_group" { 
  vpc_id = var.vpc_id 
  ingress {
      from_port = 3306 
      to_port = 3306 
      protocol = "tcp" 
      cidr_blocks = ["${var.CIDR_SEC_RDS}"]
  }
  egress {
      from_port = 0 
      to_port = 0 
      protocol = "-1"
      cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = "${var.ENVIRONMENT}_db_security_group"
  }
}