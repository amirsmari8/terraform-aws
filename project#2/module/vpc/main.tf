provider "aws" {
  region = var.REGION
} 


# The Availability Zones data source allows access to the list of AWS Availability Zones which can be accessed by an AWS account within the region configured in the provider.
# Allows to filter list of Availability Zones based on their current state. Can be either "available", "information", "impaired" or "unavailable"
data "aws_availability_zones" "available" {  
  state = "available"
}

resource "aws_vpc" "project_vpc" {
  cidr_block       = var.CIDR_BLOCK
  enable_dns_support = true 
  enable_dns_hostnames = true

  tags = {
    Name = "project_vpc_${var.ENVIRONMENT}"
  }
} 

resource "aws_subnet" "public-subnet-1" {
  vpc_id = aws_vpc.project_vpc.id 
  cidr_block = var.PUBLIC-1-CIDR-BLOCK 
  availability_zone = data.aws_availability_zones.available.names[0] 
  tags = {
      Name = "${var.ENVIRONMENT}-public-subnet-1"
  }
} 


resource "aws_subnet" "public-subnet-2" {
  vpc_id = aws_vpc.project_vpc.id 
  cidr_block = var.PUBLIC-2-CIDR-BLOCK 
  availability_zone = data.aws_availability_zones.available.names[1] 
  tags = {
      Name = "${var.ENVIRONMENT}-public-subnet-2"
  }  
} 


resource "aws_subnet" "private-subnet-1" {
  vpc_id = aws_vpc.project_vpc.id 
  cidr_block = var.PRIVATE-1-CIDR-BLOCK 
  availability_zone = data.aws_availability_zones.available.names[0] 
  tags = {
      Name = "${var.ENVIRONMENT}-private-subnet-1"
  }
} 

resource "aws_subnet" "private-subnet-2" {
  vpc_id = aws_vpc.project_vpc.id 
  cidr_block = var.PRIVATE-2-CIDR-BLOCK 
  availability_zone = data.aws_availability_zones.available.names[1] 
  tags = {
      Name = "${var.ENVIRONMENT}-private-subnet-2"
  }  
} 

resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.project_vpc.id 
  tags = {
    Name = "${var.ENVIRONMENT}-igw"
  }
} 

resource "aws_eip" "eip" {
  vpc = true 
  depends_on = [aws_internet_gateway.igw]
}


resource "aws_nat_gateway" "nat-gateway" {
    allocation_id = aws_eip.eip.id
    subnet_id = aws_subnet.public-subnet-1.id
    depends_on = [aws_internet_gateway.igw]   #TOCHECK 
    tags = {
      Name = "${var.ENVIRONMENT}-nat-gateway"
    }
}


resource "aws_route_table" "public-rtb" { 
    vpc_id = aws_vpc.project_vpc.id 
    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = aws_internet_gateway.igw.id
    }
    tags = {
      Name = "${var.ENVIRONMENT}-public-rtb"
    }  
}

resource "aws_route_table" "private-rtb" {
  vpc_id = aws_vpc.project_vpc.id
  route {
      cidr_block = "0.0.0.0/0" 
      gateway_id = aws_nat_gateway.nat-gateway.id 
  }
  tags = {
      Name  = "${var.ENVIRONMENT}-private-rtb"
  }  
}

resource "aws_route_table_association" "route_table_public_1" {
    subnet_id = aws_subnet.public-subnet-1.id 
    route_table_id = aws_route_table.public-rtb.id
}

resource "aws_route_table_association" "route_table_public_2" {
    subnet_id = aws_subnet.public-subnet-2.id
    route_table_id = aws_route_table.public-rtb.id
}

resource "aws_route_table_association" "route_table_private_1" {
    subnet_id = aws_subnet.private-subnet-1.id 
    route_table_id = aws_route_table.private-rtb.id 
}

resource "aws_route_table_association" "route_table_private_2" {
    subnet_id = aws_subnet.private-subnet-2.id 
    route_table_id = aws_route_table.private-rtb.id
}