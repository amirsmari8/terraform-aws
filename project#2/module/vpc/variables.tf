variable "REGION" {
  default = "us-east-2"
} 

variable "ENVIRONMENT" {
  default = "Development"
} 

variable "CIDR_BLOCK" {
  default = "10.0.0.0/16"
}

variable "PUBLIC-1-CIDR-BLOCK" {
  default = "10.0.101.0/24"
}

variable "PUBLIC-2-CIDR-BLOCK" {
  default = "10.0.102.0/24"
}

variable "PRIVATE-1-CIDR-BLOCK" {
  default = "10.0.1.0/24" 
}

variable "PRIVATE-2-CIDR-BLOCK" {
  default = "10.0.2.0/24"
}
