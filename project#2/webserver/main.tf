provider "aws" {
  region = var.REGION
}

module "vpc" {
  source = "../module/vpc"
  REGION = var.REGION 
  ENVIRONMENT = var.ENVIRONMENT
}

module "rds" {
  source = "../module/rds"
  REGION = var.REGION 
  ENVIRONMENT = var.ENVIRONMENT
  vpc_id = var.vpc_id
  private-subnet-1-id = var.private-subnet-1-id
  private-subnet-2-id = var.private-subnet-2-id
}


locals {
  ports = [22,80,443]
}

resource "aws_security_group" "webserver_sg" {
  name        = "${var.ENVIRONMENT}_sg_webserver"
  vpc_id      = var.vpc_id

  egress {
      from_port = 0 
      to_port = 0 
      protocol = "-1"
      cidr_blocks = ["0.0.0.0/0"]
  }
  
  dynamic "ingress" {
      for_each = local.ports 
      content {
          from_port = ingress.value 
          to_port = ingress.value 
          protocol = "tcp"
          cidr_blocks = ["${var.webserver_sg_CIDR}"]
      }
  }
  tags = {
      Name = "${var.ENVIRONMENT}_sg_webserver"
  }

}

resource "aws_key_pair" "deployer" {
  key_name = "deployer-key" 
  public_key = file(var.PATH_TO_PUBLIC_KEY)
}

resource "aws_launch_configuration" "as_conf" {
  image_id = lookup(var.AMI, var.REGION) 
  instance_type = var.instance_type 
  security_groups = [aws_security_group.webserver_sg.id]
  name = "${var.ENVIRONMENT}_as_conf"
  key_name = aws_key_pair.deployer.key_name 
  user_data = "#!/bin/bash\napt-get update\napt-get -y install net-tools nginx\nMYIP=`ifconfig | grep -E '(inet 10)|(addr:10)' | awk '{ print $2 }' | cut -d ':' -f2`\necho 'Hello Team\nThis is my IP: '$MYIP > /var/www/html/index.html"
  root_block_device {
      volume_type = "gp2"
      volume_size = 8
  }
}

resource "aws_autoscaling_group" "auto_scaling" {
    name = "${var.ENVIRONMENT}_auto_scaling" 
    max_size = 2
    min_size = 1
    desired_capacity = 1 
    vpc_zone_identifier = [var.public-subnet-1-id, var.public-subnet-2-id]
    launch_configuration = aws_launch_configuration.as_conf.name 
    target_group_arns = ["aws_lb_target_group.target_group.arn"]
    health_check_type = "EC2" 
    health_check_grace_period = 30 
}

resource "aws_lb_target_group" "target_group" {
  name     = "${var.ENVIRONMENT}-target-group"
  port     = 80
  protocol = "HTTP"
  vpc_id   = var.vpc_id
}

resource "aws_lb" "lb" {
  name               = "${var.ENVIRONMENT}-lb"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.alb_sg.id]   
  subnets = [var.public-subnet-1-id, var.public-subnet-2-id]
} 

resource "aws_lb_listener" "webserver_listner" {
  load_balancer_arn = aws_lb.lb.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    target_group_arn = aws_lb_target_group.target_group.arn
    type             = "forward"
  }
}

resource "aws_security_group" "alb_sg" {
  tags = {
    Name = "${var.ENVIRONMENT}-alb_sg"
  }
  name = "${var.ENVIRONMENT}-alb_sg"
  vpc_id      = var.vpc_id 

  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]

  }

  ingress {
    from_port = 443
    to_port = 443
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]

  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}