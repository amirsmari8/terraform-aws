variable "REGION" {
  default = "us-east-2"
}

variable "ENVIRONMENT" {
  default = "development"
}

variable "vpc_id" {
  default = ""
} 

variable "private-subnet-1-id" {
  default = ""
} 

variable "private-subnet-2-id" {
  default = ""
} 

variable "webserver_sg_CIDR" {
    default = "0.0.0.0/0"
} 

variable "PATH_TO_PUBLIC_KEY" {
    default = "~/.ssh/id_rsa.pub"
}

variable "AMI" {
  type = map(any)
  default = {
    us-east-1 = "ami-01896de1f162f0ab7"
    eu-west-2 = "ami-01d912b5940be07a5"
    us-west-1 = "ami-0189702ff9c0b592f"
    us-east-2 = "ami-045137e8d34668746"
  }
}

variable "instance_type" {
  default = "t2.micro"
}

variable "public-subnet-1-id" {
  default = ""
} 

variable "public-subnet-2-id" {
  default = ""
}