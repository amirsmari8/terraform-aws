variable "ENVIRONMENT" {
  default = "dev"
}

variable "REGION" {
  default = "us-east-2"
}