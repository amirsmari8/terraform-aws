provider "aws" {
  region = var.REGION
}

module "main_vpc" {
  source = "./module/vpc" 
  REGION = var.REGION 
  ENVIRONMENT = var.ENVIRONMENT
}

module "main_webserver" {
  source = "./webserver" 
  REGION = var.REGION 
  ENVIRONMENT = var.ENVIRONMENT 
  vpc_id = module.main_vpc.vpc_id 
  private-subnet-1-id = module.main_vpc.private-subnet-1-id
  private-subnet-2-id = module.main_vpc.private-subnet-2-id
  public-subnet-1-id = module.main_vpc.public-subnet-1-id
  public-subnet-2-id = module.main_vpc.public-subnet-2-id
}