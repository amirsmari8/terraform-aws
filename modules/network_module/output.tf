output "vpc_id" {
  value = aws_vpc.vpc.id
} 

output "security_group_id" {
  value = aws_security_group.security_groupp.id
}

output "subnet_id" {
  value = aws_subnet.public-subnet.id
}