
variable "CIDR_BLOCK" {
  default = "10.1.0.0/16"
}

variable "ENVIRONMENT" {
  default = "Production"
}

variable "SUBNET_CIDR_BLOCK" {
  default = "10.1.0.0/24"
} 

variable "AVAILABILITY_ZONE" {
  default = "us-east-2a"
}
