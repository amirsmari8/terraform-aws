# AWS vpc 
resource "aws_vpc" "vpc" {
  cidr_block       = var.CIDR_BLOCK
  instance_tenancy = "default" 
  enable_dns_support =  true 
  enable_dns_hostnames = true

  tags = {
    environment = var.ENVIRONMENT
  }
}


# AWS Internet Gateway 
resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.vpc.id

  tags = {
    environment = var.ENVIRONMENT
  }
}

# AWS SUBNET 
resource "aws_subnet" "public-subnet" {
  vpc_id     = aws_vpc.vpc.id
  cidr_block = var.SUBNET_CIDR_BLOCK
  availability_zone = var.AVAILABILITY_ZONE
  map_public_ip_on_launch = true

  tags = {
    environment = var.ENVIRONMENT
  }
}

# AWS route Table 
resource "aws_route_table" "route_table" {
  vpc_id = aws_vpc.vpc.id

  route {
    cidr_block = "0.0.0.0/0"                   
    gateway_id = aws_internet_gateway.igw.id
  }
  tags = {
    environment = var.ENVIRONMENT
  }
}    

# Table association

resource "aws_route_table_association" "rtb_subnet_public" {
  subnet_id      = aws_subnet.public-subnet.id
  route_table_id = aws_route_table.route_table.id
}

# AWS Security Group 
resource "aws_security_group" "security_groupp" {
  name = "SG_security_group"
  vpc_id = aws_vpc.vpc.id
  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks= ["0.0.0.0/0"]
  }
  egress {
    from_port = 0 
    to_port = 0 
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    environment = var.ENVIRONMENT
  }
}

