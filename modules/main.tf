
module "vpc" {
  source = "./network_module"
}

resource "aws_instance" "myec2" {
  ami = lookup(var.AMI, var.REGION) 
  instance_type = var.INSTANCE_TYPE 
  associate_public_ip_address = var.APIA 
  vpc_security_group_ids = ["${module.vpc.security_group_id}"] 
  subnet_id = module.vpc.subnet_id
  key_name = aws_key_pair.key.key_name
  tags = {
    Name = var.ENVIRONMENT
  }
}

resource "aws_key_pair" "key" {
  key_name   = "my-key"
  public_key = file(var.PATH_TO_PUBLIC_KEY)
}
