variable "REGION" {
    default = "us-east-2"
}
variable "PATH_TO_PUBLIC_KEY" {
  default = "~/.ssh/id_rsa.pub"
}

variable "AMI" {
  type = map(any)
  default = {
    us-east-1 = "ami-01896de1f162f0ab7"
    eu-west-2 = "ami-01d912b5940be07a5"
    us-west-1 = "ami-0189702ff9c0b592f"
    us-east-2 = "ami-045137e8d34668746"
  }
}

variable "INSTANCE_TYPE" {
  default = "t2.micro"
}

variable "APIA" {
  default = true
}

variable "ENVIRONMENT" {
  default = "Production"  
}