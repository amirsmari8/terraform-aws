resource "aws_instance" "myec2" {
  count         = 3 #we will have 3 instances
  ami           = lookup(var.AMI, var.REGION)
  instance_type = "t2.micro"
  tags = {
    Name = "dev-instance-${count.index}" #dev-instance-1, dev-instance-2, dev-instance-3
  }
  security_groups = var.security_group
}
