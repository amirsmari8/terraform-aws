
resource "aws_instance" "myec2" {
  count         = 3 #we will have 3 instances
  ami           = "ami-045137e8d34668746"
  instance_type = "t2.micro"
  tags = {
    Name = "dev-instance-${count.index}" #dev-instance-1, dev-instance-2, dev-instance-3
  }
}
