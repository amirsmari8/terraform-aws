
resource "aws_key_pair" "mykey" {
  key_name = "mykeys"
  public_key = file(var.PATH_TO_PUBLIC_KEY)
}

resource "aws_security_group" "allow_ssh" {
  vpc_id = var.VPC_ID
  name = "allow_ssh.${var.ENVIRONMENT}" 

  egress {
    from_port = 0
    to_port = 0 
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port = 22 
    to_port = 22 
    protocol = "tcp"
    cidr_blocks = [ "0.0.0.0/0" ]
  } 

  tags = {
    Name = "allow_ssh.${var.ENVIRONMENT}" 
    Environment = var.ENVIRONMENT
  }
} 


resource "aws_instance" "myec2" {
  ami = lookup(var.AMI, var.REGION) 
  instance_type = var.INSTANCE_TYPE 
  associate_public_ip_address = var.apia
  vpc_security_group_ids = [aws_security_group.allow_ssh.id] 
  subnet_id = element(var.public_subnet, 0)
  key_name = aws_key_pair.mykey.key_name
  tags = {
    Name = "instance-${var.ENVIRONMENT}"
  }
}