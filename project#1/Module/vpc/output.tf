output "myvpc_id" {
  value = module.vpc.vpc_id
}

output "public_subnets" {
  value = module.vpc.public_subnets     // return a list   
}

output "private_subnets" {
  value = module.vpc.private_subnets      // return a list
}