
variable "ENVIRONMENT" {
  default = ""
}

variable "REGION" {
  default = "us-east-2"
}