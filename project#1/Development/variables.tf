variable "env" {
  default = "dev"
}

variable "region" {
  default = "us-east-2"
}


variable "instance_type" {
  default = "t2.micro" 
}

variable "apia" {
  default = "true"
}

variable "path_to_public_key" {
  default = "~/.ssh/id_rsa.pub"
}