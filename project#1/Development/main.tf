provider "aws" {
  region = var.region
}


module "prod-vpc" {
  source =  "../Module/vpc" 
  ENVIRONMENT = var.env
  REGION = var.region
}

module "instance" {
  source = "../Module/instances" 
  ENVIRONMENT = var.env
  REGION = var.region
  VPC_ID = module.prod-vpc.myvpc_id  #*
  INSTANCE_TYPE = var.instance_type
  apia = var.apia
  public_subnet = module.prod-vpc.public_subnets #*
  PATH_TO_PUBLIC_KEY = var.path_to_public_key
}