provider "aws" {
  region = var.REGION
}

module "ec2_cluster" {
  name = "ec2${count.index}"   // name of the instance 
  source  = "terraform-aws-modules/ec2-instance/aws" // source from github 
  instance_type          = "t2.micro"
  ami = "ami-045137e8d34668746" 
  # count = length(var.subnet_ids) // Create one instance for each subnet
  count = var.ENVIRONMENT == "production" ? 2 : 1 // if the ENVIRONMENT is production spin up 2 instances (instance_count!) 
  subnet_id = "subnet-16f2715a"
  tags = {
    Terraform   = "true"
    Environment = var.ENVIRONMENT
  }
}