variable "REGION" {
  default = "us-east-2"
}

variable "ENVIRONMENT" {
  default = "production"
}