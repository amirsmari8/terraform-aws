- whenever you create a terraform ressources (file or directory) you need to initialize the terraform with: $terraform init

- whenever we want to save a specific plan ($terraform plan): $terraform plan -out myfirstplan.out // then, $terraform apply "myfirstplan.out"

- Terraform records informations about what ifrastructure it created in "terraform.tfstate"

- Terraform also maintain the back-up of earlier statefile in file named terraform.tfstate.backup 

- terraform.tfstate.backup + terraform.tfstate need to be store in a shared location. with team memebers , only one person need to apply changes for that we have "Locking state files" 

- Instead of using version control, the best way to manage shared storage for state files is to use Terrform built-in support for "remote beckend". a terraform backend determines how terraform loads and stores state  

- "Remote backend" (store the state file after each apply) solve probléme :manual error, locking, secret(encryption in transit + encryption on disk).

- example of remote state (backend) : s3 bucket ... 

- user data can run scripts after the instance starts 
- user can pass two types of user data : 
  ** shell scripts 
  ** cloud-init directives 

- user data are executed as the root .
- cloud-init is a software package that automates the initialization of cloud instances during system boot.
- ALB support "path based routing" and "host based routing" . 
- Terraform Modules: Terraform modules provides re-usable code. 
- Sources of Modules: Github, Terraform Resgistry, local file path. 

- Packer is an open source tool for creating identical "machine images" for multiple platform from single source configuration. 
- a machine image is a single static unit that contains a pre-configured operating system and installed software which is used to quickly create a   new running machines. 
- Packer image or machine image: PreConfigured Image (OS)+ Configuration (packages which needs to be installed) 
- Packer images allow you to launch completely provisioned and configured machine in secondes . 
- Packer components : 
  * template: configuration file used to define what image we want built 
  * builders: responsable for creating images for various platform (aws, gcp, azure) 
  * Provisioners: used to install and configure the machine image after booting 
  