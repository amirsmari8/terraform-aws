# output with List
output "for_output_list" {
  description = "for loop with list" 
  value = [for instance in aws_instance.myec2vm: instance.public_ip]
} 


# output with Map
output "for_output_map" {
  description = "for loop with map" 
  # instance.id is the key, instance.public_ip will be the value 
  value = {for instance in aws_instance.myec2vm: instance.id => instance.public_ip}
} 

# output with Map advanced
output "for_output_map_advanced" {
  description = "for loop with map_advanced" 
  # instance.id is the key, instance.public_ip will be the value 
  value = {for c, instance in aws_instance.myec2vm: c => instance.public_ip}
} 

# output legacy splat operator (*)

output "for_output_with_legacy_splat" {
  # return a list
  value = aws_instance.myec2vm[*].public_ip
 
}