# count 
count is a meta-arguments 
count have an arguments like: count.index

# list vs map 
1 list: 
Lists are ordered collections of values.
Elements in a list are accessed by their index.
example: 

variable "servers" {
  type    = list
  default = ["web", "db", "app"]
}

=> Accessing list elements: ${var.servers[0]} would return "web". 
______________

2 Map: 
Maps are collections of key-value pairs.
Elements in a map are accessed by their keys.
examle: 

variable "instances" {
  type    = map
  default = {
    "web" = "10.0.0.1"
    "db"  = "10.0.0.2"
    "app" = "10.0.0.3"
  }
}

=> Accessing map elements: ${var.instances["web"]} would return "10.0.0.1". 


==> we can use also count and the arguments "count.index" , make sur that we modify also the outputs. 

______________

there will be Output: 
  * For loop with list 
  * For loop with map 
  * legacy splat operator => *  (output only a list) (*)