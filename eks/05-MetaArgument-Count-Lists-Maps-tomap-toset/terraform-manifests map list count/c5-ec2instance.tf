resource "aws_instance" "myec2vm" {
  ami = data.aws_ami.amzlinux2.id
  # for List type 
  # instance_type = var.instance_type[1]
  # for Map type 
  instance_type = var.instance_type["dev"]
  user_data = file("${path.module}/app1-install.sh")
  key_name = var.instance_keypair
  vpc_security_group_ids = [ aws_security_group.vpc-ssh.id, aws_security_group.vpc-web.id ] 
  count = 5      # count is meta-arguments like for each ... 
  tags = {
    "Name" = "EC2 Demo 2-${count.index}"
  }
}



