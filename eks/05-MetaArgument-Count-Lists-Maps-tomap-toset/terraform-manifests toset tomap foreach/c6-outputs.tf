output "for_each_public_ip" {
  value = [for instance in aws_instance.myec2vm: instance.public_ip]
  # we can also convert the the value to a set (string) 
  # value = toset([for instance in aws_instance.myec2vm: instance.public_ip])
}

output "for_each_publicip2" {
  value = {for az, instance in aws_aws_instance.myec2vm: az => instance.public_ip}
  # we can also convert the value to a map 
  # value = tomap({for az, instance in aws_aws_instance.myec2vm: az => instance.public_ip})
}

