# for_each 
is a meta arguments , for example for each AZ we need to create an EC2 instance. 
but before that we need to get the "list of AZ" present in that  REGION (paris region) 

=> The for_each meta-argument accepts a "map" or a set of "strings", and creates an instance for each item in that map or set. (no list) 
---
the probleme here is that : "data.aws_availability_zones.my_az.names" return a "list" of AZ ==> so we need to convert it to a "set of string" using toset . 

# toset 
toset converts its arguments to as set value.
for_each = toset(data.aws_availability_zones.my_az.names) 

=> the output of "for_each" is simple = [for instance in aws_instance.myec2vm: instance.public_ip] 
# tomap 
converts its argument to a map value. 
example: 
> tomap({"a" = "foo", "b" = true})
{
  "a" = "foo"
  "b" = "true"
}

Note: see utility project