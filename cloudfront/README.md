- AWS WAF
- Web ACLs, RULES, WCU 
- WAF rules
    * managed 
        - aws managed 
        - partner managed 
    * custom rules 
    * rate limit rules 
    * ip sets 
    * regex based rules    
- REPORTING 
    * console Reporting 
    * Access LOgs 
    * Custom WAF dashboard deployment 
- Security automation with WAF 
- Pricing 

# AWS WAF 
WEB APPLICATION FIREWALL (not only for cloudfront, but also for ALB or API GATEWAY)  
NOTE: shield is the "a managed DDOS protection" for AWS  
# Web ACLs
it's a set of rules 
# ip sets  
allow us to create our own of ips for whitelist or blacklist 
