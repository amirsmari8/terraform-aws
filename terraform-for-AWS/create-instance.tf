resource "aws_instance" "dev-ec2" {
  ami = lookup(var.AMI, var.REGION)
  vpc_security_group_ids = [aws_security_group.SG.id]
  instance_type = "t2.micro"
  key_name = aws_key_pair.mykey.key_name
  subnet_id = aws_subnet.dev_vpc-public-1.id
  tags = {
    Name = "dev-instance"
  }
}

resource "aws_key_pair" "mykey" {
  key_name = "mykeys"
  public_key = file(var.PATH_TO_PUBLIC_KEY)
}