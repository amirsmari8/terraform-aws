resource "aws_instance" "dev-ec2" {
  ami = lookup(var.AMI, var.REGION)
  # vpc_security_group_ids = [aws_security_group.SG.id]
  instance_type = "t2.micro"
  key_name = aws_key_pair.mykey.key_name
  # subnet_id = aws_subnet.dev_vpc-public-1.id
  availability_zone = "us-east-2a"

  tags = {
    Name = "dev-instance"
  }
}

resource "aws_key_pair" "mykey" {
  key_name = "mykeys"
  public_key = file(var.PATH_TO_PUBLIC_KEY)
}

# EBS creation
resource "aws_ebs_volume" "seconde_ebs_volume" {
  availability_zone = "us-east-2a"
  size              = 40
  type = "gp2"

  tags = {
    Name = "seconde_ebs_volume"
  }
}

# EBS attachement to the EC2

resource "aws_volume_attachment" "ebs_att" {
  device_name = "/dev/xvdh"
  volume_id   = aws_ebs_volume.seconde_ebs_volume.id
  instance_id = aws_instance.dev-ec2.id
}

// if we tape in the instanace: $df -h , we will not see the second disk (ebs)
// for that we need first to create the file system: $mkfs.ext4 /dev/xvdh 
// then $mkdir -p /data and $mount /dev/xvdh
// if we reboot the instance , the second disk will gone , for that in the ec2 
// $vim /etc/fstab , we we will add another entry 
// /dev/xvdh /data ext4 defaults 0 0  