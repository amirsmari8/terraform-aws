resource "aws_instance" "dev-ec2" {
  ami = lookup(var.AMI, var.REGION)
  # vpc_security_group_ids = [aws_security_group.SG.id]
  instance_type = "t2.micro"
  key_name = aws_key_pair.mykey.key_name
  # subnet_id = aws_subnet.dev_vpc-public-1.id
  availability_zone = "us-east-2a"
  security_groups = [aws_security_group.sg.name]
  user_data = file("installapache.sh")

  tags = {
    Name = "dev-instance"
  }
}

resource "aws_key_pair" "mykey" {
  key_name = "mykeys"
  public_key = file(var.PATH_TO_PUBLIC_KEY)
}

locals {
  ports = [22, 80]
}
resource "aws_security_group" "sg" {
  name = "sg_5001"
  dynamic "ingress" {
    for_each = local.ports
    content {
      from_port = ingress.value 
      to_port = ingress.value
      protocol = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }
  egress {
    from_port = 0
    to_port = 0 
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}